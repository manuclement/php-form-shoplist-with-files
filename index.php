
<?php

// GLOBALS VAR

$shoplist_arr = [];
$shoplist_li = "";
$folder_path = "db";
$db_arr = array();

/////////////////////

if (!file_exists($folder_path))
    mkdir($folder_path);

if (isset($_POST["newitem"])) {
    if ($_POST["newitem"] != "") {
       $ts = time();
       file_put_contents("$folder_path/$ts.txt",$_POST["newitem"]);
    }
}

if (isset($_GET["toremove"])) {
    $txtfile = $_GET["toremove"].'.txt';
    $pathfile = "$folder_path/$txtfile";
    if (is_file($pathfile)) {
        unlink($pathfile);
    }
    header("Location:withfiles.php");
}

$folders = scandir($folder_path);
sort($folders);
foreach ($folders as $folder) {
    if ($folder !== "." && $folder !== "..") {
        $content = file_get_contents("$folder_path/$folder");
        $db_arr[] = [
            "id" => str_replace(".txt","",$folder),
            "content" => $content,
        ];
    }

}

foreach ($db_arr as $db)
    $shoplist_li.= '<li>'.$db["content"].' <a href="?toremove='.$db["id"].'">[ X ]</a></li>';


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Liste des courses</h1>

<div>
    <?php if ($shoplist_li !== "")
        echo '<ul>'.$shoplist_li.'</ul>';
    ?>
</div>

<form method="POST">
    <input type="text" name="newitem">
    <button>Ajouter</button>
</form>





</body>
</html>
